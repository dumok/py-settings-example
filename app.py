from neil_conf import settings

def main_func():
    print('-'*80)
    print('세팅파일: {}'.format(settings))
    print('이름: {}'.format(settings.NAME))
    print('dict 1: {}'.format(settings.DATABASE))
    print('dict 2: {}'.format(settings.CUSTOM_SETTINGS))
    print('list 1: {}'.format(settings.LIST_SETTING))

if __name__ == '__main__':
    main_func()
