import importlib
import argparse
import os

ENVIRONMENT_VARIABLE = 'NEIL_SETTING'

class Setting():
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--{}'.format(ENVIRONMENT_VARIABLE), help="설정 패키지명 (default: settings.develop)", default='settings.develop', type= str)
        args = parser.parse_args()
        self.settings_module = vars(args).get(ENVIRONMENT_VARIABLE)
        mod = importlib.import_module(self.settings_module)
        for setting in dir(mod):
            setting_value = getattr(mod, setting)
            setattr(self, setting, setting_value)

    def __repr__(self):
        return '<%(cls)s "%(settings_module)s">' % {
            'cls': self.__class__.__name__,
            'settings_module': self.settings_module,
        }

settings = Setting()