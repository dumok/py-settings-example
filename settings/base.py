NAME = '기본'

DATABASE = {
    'url': '디비주소',
    'id': '아이디',
    'pw': '기본패스워드'
}

LIST_SETTING = ['일', '이', '삼']

CUSTOM_SETTINGS = {
    'name': '세팅베이스',
    'url': '베이스주소',
    'key': '기본키값'
}
