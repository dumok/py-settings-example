from .base import *

NAME = '운영'

CUSTOM_SETTINGS.update({'url': '운영주소', 'key': '운영키값'})

DATABASE.update({
    'url': '운영디비주소',
    'id': '운영아이디',
    'pw': '운영패스워드'
})

LIST_SETTING += ['오']
